package com.user_order.useroderapi.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.user_order.useroderapi.models.CDrink;
import com.user_order.useroderapi.repository.IDrinkRepository;
import com.user_order.useroderapi.service.DrinkService;

@RestController
@CrossOrigin
@RequestMapping("/drinks")
public class DrinkController {
    @Autowired
    IDrinkRepository drinkRepository;
    @Autowired
    DrinkService drinkService;
    //------------------------GET------------------

    @GetMapping("/all")
    public ResponseEntity<List<CDrink>> getAllDrinks() {
        try {
            return new ResponseEntity<>(drinkService.getAllDrink(),HttpStatus.OK);            
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);   
        }
    }
   
    @GetMapping("/{id}")
	public ResponseEntity<Object> getDrinkById(@PathVariable("id") long id) {
		CDrink dataDrink = drinkService.getDrinkById(id);
		if (dataDrink != null) {
			return new ResponseEntity<>(dataDrink, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Drink not found", HttpStatus.NOT_FOUND);
		}
	}

    
    //---------------------------POST---------------------------

    @PostMapping 
	public ResponseEntity<Object> createDrinkWithService(@Valid @RequestBody CDrink drink) {
		try {
			CDrink newDrink = drinkService.createDrink(drink);
			return new ResponseEntity<>(drinkRepository.save(newDrink), HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Drink: "+e.getCause().getCause().getMessage());
		}
	}


    //------------------- PUT ---------------------------

    @PutMapping("/{id}") 
	public ResponseEntity<Object> updateDrinkByIdWithService(@PathVariable("id") long id, @Valid @RequestBody CDrink drink) {
		CDrink updatedDrink = drinkService.updateDrinkById(id, drink);
		if (updatedDrink != null) {
			try {
				return new ResponseEntity<>(drinkRepository.save(updatedDrink), HttpStatus.OK);	
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity().body("Failed to Update specified Drink:"+e.getCause().getCause().getMessage());
			}
		} else {
			return new ResponseEntity<>("Drink not found", HttpStatus.NOT_FOUND);
		}
	}


    //-------------------- DELETE -------------------------

    @DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteDrinkById(@PathVariable("id") long id) {
		try {
			drinkRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
 
    @DeleteMapping("/all")
	public ResponseEntity<Object> deleteAllDrinks() {
		try {
			drinkRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
