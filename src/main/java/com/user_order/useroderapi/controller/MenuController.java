package com.user_order.useroderapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.user_order.useroderapi.models.CMenu;
import com.user_order.useroderapi.repository.IMenuRepository;
import com.user_order.useroderapi.service.MenuService;

@RestController
@CrossOrigin
@RequestMapping()
public class MenuController {
    @Autowired
    IMenuRepository menuRepository;
    @Autowired
    MenuService menuService;
    //------------------------GET------------------

    @GetMapping("/all")
    public ResponseEntity<List<CMenu>> getAllMenus() {
        try {
            return new ResponseEntity<>(menuService.getAllMenu(),HttpStatus.OK);            
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);   
        }
    }
   
    @GetMapping("/{id}")
	public ResponseEntity<Object> getMenuById(@PathVariable("id") long id) {
		CMenu dataMenu = menuService.getMenuById(id);
		if (dataMenu != null) {
			return new ResponseEntity<>(dataMenu, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Drink not found", HttpStatus.NOT_FOUND);
		}
	}

    
    //---------------------------POST---------------------------

    @PostMapping 
	public ResponseEntity<Object> postMenu(@RequestBody CMenu menu) {
		try {
			CMenu newMenu = menuService.createMenu(menu);
			return new ResponseEntity<>(menuRepository.save(newMenu), HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Menu: "+e.getCause().getCause().getMessage());
		}
	}


    //------------------- PUT ---------------------------

    @PutMapping("/{id}") 
	public ResponseEntity<Object> putMenuById(@PathVariable("id") long id, @RequestBody CMenu menu) {
		CMenu updatedMenu = menuService.updateMenuById(id, menu);
		if (updatedMenu != null) {
			try {
				return new ResponseEntity<>(menuRepository.save(updatedMenu), HttpStatus.OK);	
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity().body("Failed to Update specified Menu:"+e.getCause().getCause().getMessage());
			}
		} else {
			return new ResponseEntity<>("Menu not found", HttpStatus.NOT_FOUND);
		}
	}


    //-------------------- DELETE -------------------------

    @DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteMenuById(@PathVariable("id") long id) {
		try {
			menuRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
 
    @DeleteMapping("/all")
	public ResponseEntity<Object> deleteAllMenus() {
		try {
			menuRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
