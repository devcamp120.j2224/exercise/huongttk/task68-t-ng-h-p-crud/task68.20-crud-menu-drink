package com.user_order.useroderapi.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "menu")
public class CMenu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "kich_co")
    private String kichCo;
    @Column(name = "duong_kinh")
    private int duongKinh;
    @Column(name = "suon")
    private int suon;
    @Column(name = "salad")
    private int salad;
    @Column(name = "so_luong_nuoc")
    private int soLuongNuoc;
    @Column(name = "thanh_tien")
    private int thanhTien;
    
    public CMenu() {
    }

    public CMenu(int id, String kichCo, int duongKinh, int suon, int salad, int soLuongNuoc, int thanhTien) {
        this.id = id;
        this.kichCo = kichCo;
        this.duongKinh = duongKinh;
        this.suon = suon;
        this.salad = salad;
        this.soLuongNuoc = soLuongNuoc;
        this.thanhTien = thanhTien;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKichCo() {
        return kichCo;
    }

    public void setKichCo(String kichCo) {
        this.kichCo = kichCo;
    }

    public int getDuongKinh() {
        return duongKinh;
    }

    public void setDuongKinh(int duongKinh) {
        this.duongKinh = duongKinh;
    }

    public int getSuon() {
        return suon;
    }

    public void setSuon(int suon) {
        this.suon = suon;
    }

    public int getSalad() {
        return salad;
    }

    public void setSalad(int salad) {
        this.salad = salad;
    }

    public int getSoLuongNuoc() {
        return soLuongNuoc;
    }

    public void setSoLuongNuoc(int soLuongNuoc) {
        this.soLuongNuoc = soLuongNuoc;
    }

    public int getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(int thanhTien) {
        this.thanhTien = thanhTien;
    }

}
