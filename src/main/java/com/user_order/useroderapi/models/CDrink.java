package com.user_order.useroderapi.models;

import java.util.Date;

import javax.persistence.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "drink")
public class CDrink {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull(message = "Nhập mã nước uống")
    @Size(min = 1, message = "Mã nước uống phải có ít nhất 1 ký tự")
    @Column(name = "ma_nuoc_uong", unique = true)
    private String maNuocUong;

    @NotNull(message = "Nhập tên nước uống")
    @Size(min = 1, message = "Tên nước uống phải có ít nhất 1 ký tự")
    @Column(name = "ten_nuoc_uong")
    private String tenNuocUong;

    @NotNull(message = "Nhập đơn giá")
    @Range(min = 1000, max = 500000, message = "Nhập giá trị từ 1000 đến 500000")
    @Column(name = "don_gia")
    private long donGia;

    @Column(name = "ghi_chu")
    private String ghiChu;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ngay_tao", nullable = true, updatable = false)
    @CreatedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date ngayTao;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ngay_cap_nhat", nullable = true)
    @LastModifiedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date ngayCapNhat;

    public CDrink() {
    }

    public CDrink(long id,
            @NotNull(message = "Nhập mã nước uống") @Size(min = 1, message = "Mã nước uống phải có ít nhất 1 ký tự") String maNuocUong,
            @NotNull(message = "Nhập tên nước uống") @Size(min = 1, message = "Tên nước uống phải có ít nhất 1 ký tự") String tenNuocUong,
            @NotNull(message = "Nhập đơn giá") @Range(min = 1000, max = 500000, message = "Nhập giá trị từ 1000 đến 500000") long donGia,
            String ghiChu, Date ngayTao, Date ngayCapNhat) {
        this.id = id;
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.donGia = donGia;
        this.ghiChu = ghiChu;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMaNuocUong() {
        return maNuocUong;
    }

    public void setMaNuocUong(String maNuocUong) {
        this.maNuocUong = maNuocUong;
    }

    public String getTenNuocUong() {
        return tenNuocUong;
    }

    public void setTenNuocUong(String tenNuocUong) {
        this.tenNuocUong = tenNuocUong;
    }

    public long getDonGia() {
        return donGia;
    }

    public void setDonGia(long donGia) {
        this.donGia = donGia;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public Date getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }

    public Date getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(Date ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }
    
}
