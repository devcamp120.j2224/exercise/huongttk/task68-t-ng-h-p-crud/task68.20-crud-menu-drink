package com.user_order.useroderapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.user_order.useroderapi.models.CDrink;

public interface IDrinkRepository extends JpaRepository <CDrink, Long>{
    
}
