package com.user_order.useroderapi.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.user_order.useroderapi.models.CUser;

@Repository
public interface IUserRepository extends JpaRepository <CUser, Long>{
   
}
