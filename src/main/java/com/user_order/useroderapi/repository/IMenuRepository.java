package com.user_order.useroderapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.user_order.useroderapi.models.CMenu;

public interface IMenuRepository extends JpaRepository <CMenu, Long>{
    
}
