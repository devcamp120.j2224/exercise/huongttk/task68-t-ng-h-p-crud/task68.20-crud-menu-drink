package com.user_order.useroderapi.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.user_order.useroderapi.models.CDrink;
import com.user_order.useroderapi.repository.IDrinkRepository;

@Service
public class DrinkService {
    @Autowired
	IDrinkRepository drinkRepository;

    //----------------GET---------------------

    public List<CDrink> getAllDrink() {
        List<CDrink> listDrink = new ArrayList<CDrink>();
        drinkRepository.findAll().forEach(listDrink::add);
        return listDrink;
    }
    
    public CDrink getDrinkById(long id) {
        Optional<CDrink> dataDrink = drinkRepository.findById(id);
        if (dataDrink.isPresent()) {
			return dataDrink.get();
		} else {
			return null;
		}
    }


    //--------------------POST----------------------

    public CDrink createDrink(CDrink pDrink) {
        CDrink newDrink = new CDrink();
        newDrink.setMaNuocUong(pDrink.getMaNuocUong());
        newDrink.setTenNuocUong(pDrink.getTenNuocUong());
        newDrink.setDonGia(pDrink.getDonGia());
        newDrink.setGhiChu(pDrink.getGhiChu());
        newDrink.setNgayTao(new Date());
        newDrink.setNgayCapNhat(null);            
        return newDrink;
    }

    //------------------------PUT------------------------------
    
    public CDrink updateDrinkById(long id, CDrink pDrink) {
        Optional<CDrink> dataDrink = drinkRepository.findById(id);
		if (dataDrink.isPresent()) {
			CDrink updatedDrink = dataDrink.get();
			updatedDrink.setMaNuocUong(pDrink.getMaNuocUong());
            updatedDrink.setTenNuocUong(pDrink.getTenNuocUong());
			updatedDrink.setDonGia(pDrink.getDonGia());
            updatedDrink.setGhiChu(pDrink.getGhiChu());
			updatedDrink.setNgayCapNhat(new Date());
            return updatedDrink;
		} else {
            return null;
        }
    }
}
