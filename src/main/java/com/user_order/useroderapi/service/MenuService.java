package com.user_order.useroderapi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.user_order.useroderapi.models.CMenu;
import com.user_order.useroderapi.repository.IMenuRepository;

@Service
public class MenuService {
    @Autowired
	IMenuRepository menuRepository;

    //----------------GET---------------------

    public List<CMenu> getAllMenu() {
        List<CMenu> listMenu = new ArrayList<CMenu>();
        menuRepository.findAll().forEach(listMenu::add);
        return listMenu;
    }
    
    public CMenu getMenuById(long id) {
        Optional<CMenu> dataMenu = menuRepository.findById(id);
        if (dataMenu.isPresent()) {
			return dataMenu.get();
		} else {
			return null;
		}
    }


    //--------------------POST----------------------

    public CMenu createMenu(CMenu menu) {
        CMenu newMenu = new CMenu();
        newMenu.setDuongKinh(menu.getDuongKinh());
        newMenu.setKichCo(menu.getKichCo());
        newMenu.setSalad(menu.getSalad());
        newMenu.setSuon(menu.getSuon());
        newMenu.setSoLuongNuoc(menu.getSoLuongNuoc());
        newMenu.setThanhTien(menu.getThanhTien());           
        return newMenu;
    }

    //------------------------PUT------------------------------
    
    public CMenu updateMenuById(long id, CMenu menu) {
        Optional<CMenu> dataMenu = menuRepository.findById(id);
		if (dataMenu.isPresent()) {
			CMenu updatedMenu = dataMenu.get();
            updatedMenu.setDuongKinh(menu.getDuongKinh());
            updatedMenu.setKichCo(menu.getKichCo());
            updatedMenu.setSalad(menu.getSalad());
            updatedMenu.setSuon(menu.getSuon());
            updatedMenu.setSoLuongNuoc(menu.getSoLuongNuoc());
            updatedMenu.setThanhTien(menu.getThanhTien());   
            return updatedMenu;
		} else {
            return null;
        }
    }
}
