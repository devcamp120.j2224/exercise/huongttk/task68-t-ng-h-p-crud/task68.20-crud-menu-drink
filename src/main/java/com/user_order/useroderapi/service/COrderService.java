package com.user_order.useroderapi.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.user_order.useroderapi.models.COrder;
import com.user_order.useroderapi.models.CUser;
import com.user_order.useroderapi.repository.IOrderRepository;
import com.user_order.useroderapi.repository.IUserRepository;


@Service
public class COrderService {
    @Autowired
    //-------------IMPOTR INTERFACE--------------
    IOrderRepository orderRepository;
    @Autowired
    IUserRepository userRepository;


    //-------------GET---------------------

    public ResponseEntity <List<COrder>> getAllOrder() {
        try {
            List <COrder> listOrder = new ArrayList<COrder>();
            orderRepository.findAll().forEach(listOrder :: add);
            return new ResponseEntity<>(listOrder, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity <Object> getOrderById(long id) {
        Optional<COrder> orderData = orderRepository.findById(id);
        if (orderData.isPresent()) {
            return new ResponseEntity<>(orderData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //GET ORDER BY USERID------------
/* 
    public ResponseEntity<Object> getOrderByUserId(long userId) {
        Optional<CUser> userData = userRepository.findById(userId);
		if (userData.isPresent())
			return new ResponseEntity<>(userData.get().getOrders(), HttpStatus.OK);
		else
			return new ResponseEntity<>("PhongBan not found", HttpStatus.NOT_FOUND);
    }
*/

    //-------------------------POST-----------------------

    public ResponseEntity<Object> createOrder(long userId, COrder order) {
        try {
            Optional<CUser> userData = userRepository.findById(userId);
            if (userData.isPresent()) {
                COrder newOrder = new COrder();
                newOrder.setPizzaSize(order.getPizzaSize());
                newOrder.setPizzaType(order.getPizzaType());
                newOrder.setPrice(order.getPrice());
                newOrder.setPaid(order.getPaid());
                newOrder.setVoucherCode(order.getVoucherCode());
                newOrder.setCreated(new Date());
                newOrder.setUser(userData.get());
                return new ResponseEntity<>(orderRepository.save(newOrder), HttpStatus.CREATED);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    //------------- PUT--------------------

    public ResponseEntity<Object> updateOrderById(long id, COrder order) {
        try {
            Optional<COrder> orderData = orderRepository.findById(id);

            if (orderData.isPresent()) {
                COrder orderUpdate = orderData.get();
                orderUpdate.setPizzaSize(order.getPizzaSize());
                orderUpdate.setPizzaType(order.getPizzaType());
                orderUpdate.setPrice(order.getPrice());
                orderUpdate.setPaid(order.getPaid());
                orderUpdate.setVoucherCode(order.getVoucherCode());
                orderUpdate.setUpdated(new Date());
                return new ResponseEntity<>(orderRepository.save(orderUpdate), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //////////////////////////// DELETE SERVICES///////////////////////////
    public ResponseEntity<Object> deleteOrderById(long id) {
        try {
           orderRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> deleteOrder() {
        try {
           orderRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
